# JSON

## Definition
JSON stands for JavaScript Object Notation. It's a lightweight data interchange format that is easy for humans to read and write.

JSON is often used to transmit data between a server and a web application as a text-based format.
JSON is a subset of the JavaScript programming language but it's language-independent, meaning it can be used with many programming languages.
JSON is structured as key-value pairs and can represent data in a hierarchical format using arrays and objects.

## Key Points

JSON Must be in key pair format "{}".
It can be hierarchically arranaged.
JSON key must be in double quote(""),not single quote or non quote.
Key Must be a string with double quote.
Value can be only string,boolean,number,null,array,object.



## Syntax

```
{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}

```


# Important Functions
## JSON.parse():
Purpose: Converts a JSON string into a JavaScript object.
Usage: JSON.parse(jsonString)
```
const jsonString = '{"name": "John", "age": 30}';
const obj = JSON.parse(jsonString);
console.log(obj); // Output: { name: 'John', age: 30 }

```

## JSON.stringify():
Purpose: Converts a JavaScript object into a JSON string.
Usage: JSON.stringify(jsonObject)

```
const obj = { name: 'John', age: 30 };
const jsonString = JSON.stringify(obj);
console.log(jsonString); // Output: '{"name":"John","age":30}'


```

## Data Type

JSON supports six data types


```
String: A sequence of characters, represented in double quotes. For example: "Hello, World!"

Number: A numeric value, which can be an integer or a floating-point number. For example: 42 or 3.14

Boolean: Represents either true or false.

Array: An ordered list of values enclosed in square brackets []. For example: [1, 2, 3] or ["apple", "orange", "banana"]

Object: An unordered collection of key-value pairs enclosed in curly braces {}. For example: {"key1": "value1", "key2": "value2"}

null: Represents an empty value. For example: null
```
The reasion to use only above data type is that all the programming supports these data types.

Note: JSON values can not be given type.
Function
Date
undefined


## Accessing String

```
const jsonData = `{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}
`;
const data = JSON.parse(jsonData); // Convert the JSON string to a JavaScript object
const name = data.name; // Accessing the value associated with the key "name"
console.log(name); // This would output: "John Doe"

```

## Accessing Number

```
const jsonData = `{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}
`;
const data = JSON.parse(jsonData); // Convert the JSON string to a JavaScript object
const age = data.age; // Accessing the value associated with the key "age"
console.log(age); // This would output: 30

```


## Accessing Boolean


```
const jsonData = `{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}
`;
const data = JSON.parse(jsonData); // Convert the JSON string to a JavaScript object
const isStudent = data.isStudent; // Accessing the value associated with the key "isStudent"
console.log(isStudent); // This would output: false

```


## Accessing Array

```
const jsonData = `{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}
`;
const data = JSON.parse(jsonData); // Convert the JSON string to a JavaScript object
const firstLanguage = data.languages[0]; // Accessing the first element in the array
console.log(firstLanguage); // This would output: "English"

const secondLanguage = data.languages[1]; // Accessing the second element in the array
console.log(secondLanguage); // This would output: "Spanish"

```


## Accessing Object

```
const jsonData = `{
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
}`;
const data = JSON.parse(jsonData); // Convert the JSON string to a JavaScript object
const city = data.address.city; // Accessing the value associated with the key "city" within the "address" object
console.log(city); // This would output: "New York"

const zipcode = data.address.zipcode; // Accessing the value associated with the key "zipcode" within the "address" object
console.log(zipcode); // This would output: "10001"


```



# Accessing JSON Values via different types of loop [Only For Object].



## Using for...in loop (for objects)


```
const jsonData = {
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
};

for (let key in jsonData) {
    console.log(`${key}: ${jsonData[key]}`);
}

```

## Using for...in with hasOwnProperty() (for objects) 

```
const jsonData = {
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
};

for (let key in jsonData) {
    if (jsonData.hasOwnProperty(key)) {
        console.log(`${key}: ${jsonData[key]}`);
    }
}
```

# Accessing JSON Values via different types of loop [Only For Array].


## Using forEach loop (for arrays):

```
const jsonData = {
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
};

const languagesArray = jsonData.languages;  // note here laguages are in array form.

languagesArray.forEach((language, index) => {
    console.log(`Language ${index + 1}: ${language}`);
});
```

## Using forEach loop (for arrays):

```
const jsonData = {
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
};

const languagesArray = jsonData.languages;  // note here laguages are in array form.

for (let i = 0; i < languagesArray.length; i++) {
    console.log(`Language ${i + 1}: ${languagesArray[i]}`);
}

```


## Using for...of loop (for arrays):


```
const jsonData = {
    "name": "John Doe",
    "age": 30,
    "isStudent": false,
    "address": {
        "city": "New York",
        "zipcode": "10001"
    },
    "languages": ["English", "Spanish"]
};

const languagesArray = jsonData.languages;  // note here laguages are in array form.

for (let language of languagesArray) {
    console.log(language);
}
```


# Object

## Definition
objects are reference data type or non-primitive data type,It used to store collections of key-value pairs.

## Key Points

Javascript Object can be in curly pair format "{}".
It can be hierarchically arranaged.
Javascript Object's key can be in double quote("") and without quote but not in single quote.
Double Quotes: In JavaScript, keys can always be enclosed in double quotes.
```
let obj = {
    "key1": "value1",
    "key2": "value2"
};

```
Single Quotes:In Javascript , keys can't be enclosed in single quotes, but can be accessible by single quote.

```
let obj = {
    "key1": "value1",
    "key2": "value2"
};

console.log(obj['key1']); // Accessing with single quotes
```
Without Quotes: If the key is a valid JavaScript identifier (e.g., doesn't start with a number, doesn't contain special characters or spaces), quotes are not necessary.
```
let obj = {
    key1: "value1",
    key2: "value2"
};

```
Note: Mostly , Without quote are the best way if needed you can use double quote.




## Syntax
 There are 2 ways to create Object.
 1 : By Using literal

```
// Empty Object
 let v1={};

// With Properties

let obj = {
    key1: "value1",
    key2: "value2"
};

```
2 : By Using Object Contructor [Generic Object Contructor].

```
// Empty Object
let obj =new Object();

// With Properties
let x=new Object();
x.name="Test";
x.email="test@gmail.com";

```
3 : By Using Contructor Function.

```
function Person(name, age) {
    this.name = name;
    this.age = age;
}

let person = new Person('Alice', 25);

console.log(person);
```

## Data Type

Object can holds given kinds of data types.

Object can holds both primitive or non-primitive types.
1 : Null
2 : Undefined
3 : Boolean
4 : Number
5 : BigInt
6 : Symbol
7 : String
8 : Object
9 : Array
10: Function

```
const mySymbol = Symbol('description');
const bigdata = BigInt("9007199254740991");

let carObject = {
    brand: "Hello", // Brand of the car
    horsePower: 42, // Engine horsepower
    isElectric: true, // Boolean indicating if it's an electric car
    fuelType: null, // Type of fuel used (null for unknown)
    transmission: undefined, // Type of transmission (undefined for unknown)
    features: [ // Array of features the car has
        "Feature 1",
        "Feature 2",
        "Feature 3"
    ],
    startEngine: function() { // Method to start the car's engine
        return "Engine started";
    },
    specifications: { // Nested object with detailed specifications
        weight: "value" // Weight of the car
    },
    [mySymbol]: "Description", // Symbolic representation of car registration
    data: bigdata // BigInt representing big data related to the car
};

```


# Getting Value from carObject

```
const mySymbol = Symbol('description');
const bigdata = BigInt("9007199254740991");

let carObject = {
    brand: "Hello", // Brand of the car
    horsePower: 42, // Engine horsepower
    isElectric: true, // Boolean indicating if it's an electric car
    fuelType: null, // Type of fuel used (null for unknown)
    transmission: undefined, // Type of transmission (undefined for unknown)
    features: [ // Array of features the car has
        "Feature 1",
        "Feature 2",
        "Feature 3"
    ],
    startEngine: function() { // Method to start the car's engine
        return "Engine started";
    },
    specifications: { // Nested object with detailed specifications
        weight: "value" // Weight of the car
    },
    [mySymbol]: "Description", // Symbolic representation of car registration
    data: bigdata // BigInt representing big data related to the car
};
```

## Getting value via Dot Notation:

```
console.log(carObject.brand); // Output: Hello
console.log(carObject.horsePower); // Output: 42
console.log(carObject.isElectric); // Output: true
console.log(carObject.fuelType); // Output: null
console.log(carObject.transmission); // Output: undefined
console.log(carObject.features); // Output: ["Feature 1", "Feature 2", "Feature 3"]
console.log(carObject.startEngine()); // Output: Engine started
console.log(carObject.specifications.weight); // Output: value
console.log(carObject[mySymbol]); // Output: Description
console.log(carObject.data); // Output: 9007199254740991n

```

## Getting Value via Square Bracket Notation:


```
console.log(carObject['brand']); // Output: Hello
console.log(carObject['horsePower']); // Output: 42
console.log(carObject['isElectric']); // Output: true
console.log(carObject['fuelType']); // Output: null
console.log(carObject['transmission']); // Output: undefined
console.log(carObject['features']); // Output: ["Feature 1", "Feature 2", "Feature 3"]
console.log(carObject['startEngine']()); // Output: Engine started
console.log(carObject['specifications']['weight']); // Output: value
console.log(carObject[mySymbol]); // Output: Description
console.log(carObject['data']); // Output: 9007199254740991n

```

Note : When object key is not valid identifier so that I need to use Square Bracket Notation to get value.
example: 
```
let car={
    name: "BMW",
    "Car Description": "BMW Description"
};

We are unable to "Car Description" value via Dot Notation because the key is not standard format.

so we need to get Square bracket notation.

console.log(car["Car Description"]); ==> output : BMW Description

```

# Basic Operations

## With Object Literals
Adding: Use dot notation or square brackets to assign a new property to the object.
Updating: Access the property and assign a new value.
Deleting: Utilize the delete keyword followed by the property name to remove it 

```
// Creating an object representing a person
let person = {
    name: 'John',
    age: 30,
    city: 'New York'
};

// Adding a new property
person.gender = 'Male';

// Updating an existing property
person.age = 31;

// Deleting a property
delete person.city;

console.log(person); // Output the updated object

```

## With Object Contructor (new Object() Generic Constuctor)
Adding: Use dot notation or square brackets to assign a new property to the object.
Updating: Access the property and assign a new value.
Deleting: Utilize the delete keyword followed by the property name to remove it 
```
// Creating an empty object using new Object()
let person1 = new Object();

// Adding properties to the object
person1.name = 'John';
person1.age = 30;
person1.city = 'New York';

// Adding a new property
person1.gender = 'Male';

// Updating an existing property
person1.age = 31;

// Deleting a property
delete person1.city;

console.log(person1); // Output the updated object

```

## With Contructor Function
Adding: Use dot notation or square brackets to assign a new property to the object.
Updating: Access the property and assign a new value.
Deleting: Utilize the delete keyword followed by the property name to remove it 
```
function Person(name, age) {
    this.name = name;
    this.age = age;
}

let person = new Person('Alice', 25);

// Adding a new property
person.newProperty = 'Some value';
console.log(person);

// Deleting a property
delete person.age;
console.log(person);

// Updating a property
person.age = 26;
console.log(person);

```



# Object Destructuring

## Single Level Destructuring

```
const mySymbol = Symbol('description');
const bigdata = BigInt("9007199254740991");

let carObject = {
    brand: "Hello", // Brand of the car
    horsePower: 42, // Engine horsepower
    isElectric: true, // Boolean indicating if it's an electric car
    fuelType: null, // Type of fuel used (null for unknown)
    transmission: undefined, // Type of transmission (undefined for unknown)
    features: [ // Array of features the car has
        "Feature 1",
        "Feature 2",
        "Feature 3"
    ],
    startEngine: function() { // Method to start the car's engine
        return "Engine started";
    },
    specifications: { // Nested object with detailed specifications
        weight: "value" // Weight of the car
    },
    [mySymbol]: "Description", // Symbolic representation of car registration
    data: bigdata // BigInt representing big data related to the car
};

const {
  brand,
  horsePower,
  isElectric,
  fuelType,
  transmission,
  features,
  startEngine,
  specifications,
  [mySymbol]: symbolic,
  data: bigData,
} = carObject;

console.log(brand); // Output: Hello
console.log(horsePower); // Output: 42
console.log(isElectric); // Output: true
console.log(fuelType); // Output: null
console.log(transmission); // Output: undefined
console.log(features); // Output: ["Feature 1", "Feature 2", "Feature 3"]
console.log(startEngine()); // Output: Engine started
console.log(specifications.weight); // Output: value
console.log(symbolic); // Output: Description
console.log(bigData); // Output: 9007199254740991n

```


## Depth Level Destructuring
To use depth destructuring, I need to use colon (:) sign on every level
```
let carObject = {
       features: [ // Array of features the car has
        "Feature 1",
        "Feature 2",
        "Feature 3"
    ],
    specifications: { // Nested object with detailed specifications
        weight: "value" ,
    }
};

const { features: [feature1, feature2, feature3], specifications: { weight } } = carObject;

console.log(feature1); // Output: "Feature 1"
console.log(feature2); // Output: "Feature 2"
console.log(feature3); // Output: "Feature 3"
console.log(weight); // Output: "value"

```


## Renaming In Destructuring
To rename , I need to use colon (:) sign

```
const object = {
  name: "Test",
  features: [
    "Feature 1",
    "Feature 2",
    "Feature 3"
  ],
  specifications: {
    weight: "value",
  }
};

const { name: objectName, features: [feature1, feature2, feature3], specifications: { weight: carWeight } } = object;

console.log(objectName); // Output: "Test"
console.log(feature1); // Output: "Feature 1"
console.log(feature2); // Output: "Feature 2"
console.log(feature3); // Output: "Feature 3"
console.log(carWeight); // Output: "value"

```

## Destructuring with function

```
// Function that takes an object as a parameter and uses destructuring
function displayInfo({ name, age }) {
  console.log(`Name: ${name}, Age: ${age}`);
}


// Calling the function and passing the object
displayInfo({
  name: 'Bob',
  age: 25
});

```

## Call the object value inside object function.

```
const person = {
  name: 'Alice',
  age: 30,
  greet: function() {
    console.log(`Hello, my name is ${this.name}!`);
  }
};

person.greet(); // Output: Hello, my name is Alice!

```


# Important Methods

## Object.assign()
The Object.assign() static method copies all own properties from one or more source objects to a target object. It returns the modified target object.

Syntax: 

```
Object.assign(target)
Object.assign(target, source1)
Object.assign(target, source1, source2)
Object.assign(target, source1, source2, /* …, */ sourceN)

```
Examples: 

```
Example: 1
 

const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// Expected output: Object { a: 1, b: 4, c: 5 }
Note : In this method, source matched key data override on target and rest data added.

console.log(returnedTarget === target);
// Expected output: true



Example 2: Cloning Object

const obj = { a: 1 };
const copy = Object.assign({}, obj);
console.log(copy); // { a: 1 }


Example 3: Warning for Deep Clone

Note: if I tried to change any value of target object or returned object, It must not be changed source value.
const obj1 = { a: 0, b: { c: 0 } };
const obj2 = Object.assign({}, obj1);
console.log(obj2); // { a: 0, b: { c: 0 } }

obj1.a = 1;
console.log(obj1); // { a: 1, b: { c: 0 } }  // here changes reflect only source
console.log(obj2); // { a: 0, b: { c: 0 } }

obj2.a = 2;
console.log(obj1); // { a: 1, b: { c: 0 } }
console.log(obj2); // { a: 2, b: { c: 0 } } // here changes reflect only returned object 

It works on first level. 

But I tried to change deep lavel object value, It'll modify source object value.

obj2.b.c = 3;  // but on deep level it change both object value as c:3
console.log(obj1); // { a: 1, b: { c: 3 } } 
console.log(obj2); // { a: 2, b: { c: 3 } }  


To fix this issue, I need to use structuredClone() function to deep cloning.

const obj3 = { a: 0, b: { c: 0 } };
const obj4 = structuredClone(obj3);
obj3.a = 4;
obj3.b.c = 4;
console.log(obj4); // { a: 0, b: { c: 0 } }


Example 4: Merging objects

const o1 = { a: 1 };
const o2 = { b: 2 };
const o3 = { c: 3 };

const obj = Object.assign(o1, o2, o3);
console.log(obj); // { a: 1, b: 2, c: 3 }
console.log(o1); // { a: 1, b: 2, c: 3 }, target object itself is changed.


Note: Issue with merging objects with same properties.

In this case , if the key or property is same of target object and source object, source value override on target value.

const o1 = { a: 1, b: 1, c: 1 };
const o2 = { b: 2, c: 2 };
const o3 = { c: 3 };

const obj = Object.assign({}, o1, o2, o3);
console.log(obj); // { a: 1, b: 2, c: 3 }


Example 5: Primitives will be wrapped to objects

const v1 = "abc";
const v2 = true;
const v3 = 10;
const v4 = Symbol("foo");

const obj = Object.assign({}, v1, null, v2, undefined, v3, v4);
Note 1:  Primitives will be wrapped, null and undefined will be ignored.
Note 2:  only string wrappers can have own properties.
console.log(obj); // { "0": "a", "1": "b", "2": "c" }

More Examples: 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign


```

## Object.create()

The Object.create() static method creates a new object, using an existing object as the prototype of the newly created object.

Example:

```
const person = {
  isHuman: false,
  printIntroduction: function () {
    console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
  },
};

const me = Object.create(person);

me.name = 'Matthew'; // "name" is a property set on "me", but not on "person"
me.isHuman = true; // Inherited properties can be overwritten

me.printIntroduction();
// Expected output: "My name is Matthew. Am I human? true"

```

## Object.entries()

The Object.entries() static method returns an array of [key,value] arrays of given object.

It returns key and value both.

Example: 

```
Basic Example 1: 
const object1 = {
  a: 'somestring',
  b: 42,
};
let x=Object.entries(object1);
console.log(x); 
OutPut: 
[
  ["a","somestring" ],
  [ "b",42 ]
]

Example 2:  with for ... of loop.

const object1 = {
  a: 'somestring',
  b: 42,
};

for (const [key, value] of Object.entries(object1)) {
  console.log(`${key}: ${value}`);
}

// Expected output:
// "a: somestring"
// "b: 42"


```

## Object.keys()
The Object.keys() static method returns an array of a given object's properties or keys in the form of string.
It work with Object and Array Both 

```
const object1 = {
  a: 'somestring',
  b: 42,
  c: false,
};

console.log(Object.keys(object1));
// Expected output: Array ["a", "b", "c"]

// Simple array
const arr = ["a", "b", "c"];
console.log(Object.keys(arr)); // ['0', '1', '2']

// Array-like object
const obj = { 0: "a", 1: "b", 2: "c" };
console.log(Object.keys(obj)); // ['0', '1', '2']

```


## Object.values()

The Object.values() static method returns an array of a given object's values.
It works with Object and string as well.

```
const object1 = {
  a: 'somestring',
  b: 42,
  c: false,
};

console.log(Object.values(object1));
// Expected output: Array ["somestring", 42, false]

Example 2: 

const obj = { foo: "bar", baz: 42 };
console.log(Object.values(obj)); // ['bar', 42]

// Array-like object
const arrayLikeObj1 = { 0: "a", 1: "b", 2: "c" };
console.log(Object.values(arrayLikeObj1)); // ['a', 'b', 'c']

// Array-like object with random key ordering
// When using numeric keys, the values are returned in the keys' numerical order
const arrayLikeObj2 = { 100: "a", 2: "b", 7: "c" };
console.log(Object.values(arrayLikeObj2)); // ['b', 'c', 'a']

```

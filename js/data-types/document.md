# Data types
Data Types are used to hold different types of values. There are 2 kinds of data types.

1: Primitive Data type.
2: Non Primitive Data type or Reference data type or Objects Data type 

## Primitive Data Type:
   Primitive data types hold their actual values, like numbers or strings, directly in the variable's memory space.

There are 7 kinds of primitive data type.
1: Null  : Represent absence of an object.
```
let obj = null; // Using null to signify absence of an object
console.log(obj); // Output: null

```

2: undefined : Represents a variable that has been declared but not assigned any value.
```
let x; // Defaults to undefined
console.log(x); // Output: undefined

```
3: Boolean : Represents a logical entity and can have two values: true or false.
```
let x=true; 
console.log(x); // Output: true

let y=false; 
console.log(y); // Output: false
```
4: Symbol

5: String :  Represents a set of characters, enclosed in either single quotes ('') or double quotes ("").
```
let x="Test"; 
console.log(x); // Output: Test

```
6: Number : Represents both integer and floating-point numbers. It includes whole numbers and decimals.
```
// With decimals:
let x1 = 34.00;

// Without decimals:
let x2 = 34;

```
7: BigInt : JavaScript BigInt variables are used to store big integer values that are too big to be represented by a normal JavaScript Number. Where All JavaScript numbers are stored in a a 64-bit floating-point format.
```
let x = BigInt("123456789012345678901234567890");
```


## Non Primitive Data Type or Reference Data Type or Objects Data Type

   Non-primitive types, such as objects or arrays, store references (memory addresses) to where the actual data is stored.

1: Object 
```
let x = {
    a:"T1",
    b:"T2"
};
```
2: Array
```
let x = ['T1','T2','T3'];
```
3: Function  
```
let x = () => 10;
``` 
